import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.router.{BreederRouter, PlantRouter, StrainRouter}
import akka.stream.ActorMaterializer
import mongo.Mongo
import monix.execution.Scheduler.Implicits.global
import repositories.{BreederCollection, PlantCollection, StrainCollection}

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{Failure, Success}

object Main extends App {
  val host = "0.0.0.0"
  val port = 9000

  implicit val system: ActorSystem = ActorSystem(name = "mongo-final")
  implicit val materializer: ActorMaterializer = ActorMaterializer()

  implicit val breederRepo = new BreederCollection(Mongo.database)
  implicit val strainRepo  = new StrainCollection(Mongo.database)
  implicit val plantRepo   = new PlantCollection(Mongo.database)

  val breederRoute = new BreederRouter
  val strainRoute  = new StrainRouter
  val plantRoute   = new PlantRouter

  val binding = Http().bindAndHandle(plantRoute.route ~ breederRoute.route ~ strainRoute.route, host, port)

  binding.onComplete {
    case Success(_) => println("Success")
    case Failure(exception) => println(s"Failed: ${exception.getMessage}")
  }

  Await.result(binding, 3.seconds)
}
