package crud

import java.util.UUID

import monix.eval.Task

import scala.reflect.ClassTag

trait CrudService[A] {
  def post(item: A): Task[A]

  def getById(id: UUID): Task[Option[A]]

  def getById[B : ClassTag](id: UUID, fields: List[String]): Task[Option[B]]

  def getMany[B : ClassTag](orderBy: List[(String, Int)],
                                           limit:   Int = 50,
                                           offset:  Int = 0,
                                           fields:  List[String]): Task[(List[B], Long)]

  def replace(id: UUID, item: A): Task[A]

  def delete(id: UUID): Task[A]

  def count(): Task[Long]
}
