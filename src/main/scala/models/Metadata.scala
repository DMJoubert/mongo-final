package models

import akka.http.scaladsl.model.StatusCodes

sealed trait Metadata {
  val status: Int
  val message: String
}

final case class CountableMetadata(status: Int, message: String, count: Long) extends Metadata
final case class UncountableMetadata(status: Int, message: String) extends Metadata

object Metadata {
  def OK(count: Long)     = CountableMetadata(StatusCodes.OK.intValue, "OK", count)

  val OK                  = UncountableMetadata(StatusCodes.OK.intValue, "OK")

  val Created             = UncountableMetadata(StatusCodes.Created.intValue, "Created")

  val NotFound            = UncountableMetadata(StatusCodes.NotFound.intValue, "Not found")

  val InternalServerError = UncountableMetadata(StatusCodes.InternalServerError.intValue, "Internal server error")
}