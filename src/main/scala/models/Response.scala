package models

sealed trait Response {
  val meta: Metadata
}

final case class EmptyResponse(meta: Metadata) extends Response
final case class DataResponse[A](data: A, meta: Metadata) extends Response

object Response {
  def single[T](data: T, meta: Metadata)                                     = DataResponse(data, meta)
  def empty(meta: Metadata)                                                  = EmptyResponse(meta)
  def list[M[X] <: TraversableOnce[X], T](data: M[T], meta: models.Metadata) = DataResponse(data, meta)
}