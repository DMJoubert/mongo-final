package models

import java.util.UUID

case class Strain(id: UUID, name: String, breederId: UUID)

object Strain {
  case class Project(id: Option[UUID], name: Option[String], breederId: Option[UUID])

  case class Create(breederId: UUID, name: String)

  case class Modify(breederId: Option[UUID], name: Option[String])
}


