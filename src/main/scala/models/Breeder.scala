package models

import java.util.UUID

/**
  * Base Breeder model used in the BreederCollection
  *
  * @param id       UUID representation of the Breeder
  * @param name     Name of the Breeder
  */
case class Breeder(id: UUID,
                   name: String)

object Breeder {
  /**
    * Breeder model used when posting a new Breeder to the BreederCollection
    *
    * @param name     Name of the Breeder that needs to be posted
    */
  case class Create(name: String)

  /**
    * Breeder model used when modifying a Breeder in the BreederCollection
    *
    * @param name     Optional value which is set to Some if the parameter needs to be changed in the BreederCollection
    */
  case class Modify(name: Option[String])

  /**
    * models.Breeder model used when projecting specific fields from the repositories.BreederRepo
    *
    * @param id       UUID of the models.Breeder encapsulated in an Option
    * @param name     Name of the models.Breeder encapsulated in an Option
    */
  case class Project(id: Option[UUID],
                     name: Option[String])
}