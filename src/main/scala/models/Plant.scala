package models

import java.time.OffsetDateTime
import java.util.UUID

case class Plant(id: UUID, strainId: UUID, startDate: OffsetDateTime)

object Plant {
  case class Modify(strainId: Option[UUID], startDate: Option[OffsetDateTime])

  case class Create(strainId: UUID, startDate: Option[OffsetDateTime])

  case class Project(id: Option[UUID], strainId: Option[UUID], startDate: Option[OffsetDateTime])
}