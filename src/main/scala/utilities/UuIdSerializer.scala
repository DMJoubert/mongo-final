package utilities
import java.util.UUID

import org.json4s.{CustomSerializer, JNull, JString}

object UuIdSerializer extends CustomSerializer[UUID](_ => (
  {
    case JString(s) => UUID.fromString(s)
    case JNull => null
  },
  {
    case d: UUID => JString(d.toString)
  }
))