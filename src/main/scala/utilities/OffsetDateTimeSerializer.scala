package utilities

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

import org.json4s.{CustomSerializer, JNull, JString}

object OffsetDateTimeSerializer extends CustomSerializer[OffsetDateTime](_ => (
  {
    case JString(s) => OffsetDateTime.parse(s)
    case JNull => null
  },
  {
    case d: OffsetDateTime => JString(d.withNano(0).format(DateTimeFormatter.ISO_OFFSET_DATE_TIME))
  }
))