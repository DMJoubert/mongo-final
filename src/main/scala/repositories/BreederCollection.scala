package repositories

import java.util.UUID

import models.Breeder
import mongo.MongoCrudService
import monix.eval.Task
import monix.execution.Scheduler
import org.mongodb.scala.MongoDatabase
import repositories.BreederCollection.BreederNotFound
import repositories.errors.NotFound

class BreederCollection(database: MongoDatabase)
                       (implicit sch: Scheduler) extends MongoCrudService[Breeder] {

  override val collection = database.getCollection[Breeder]("breeders")

  /**
    * Finds a Breeder entry inside the BreederCollection and returns it if it is found otherwise returns a
    * NotFound error
    *
    * @param id UUID of Breeder to be found
    * @return   Found Breeder or NotFound exception
    */
  def findByIdWithError(id: UUID): Task[Either[BreederNotFound, Breeder]] = {
    getById(id).map {
      case Some(breeder) => Right(breeder)
      case None          => Left(BreederNotFound(id))
    }
  }

  /**
    * Inserts an item into the BreederCollection
    *
    * @param item Item to be inserted in the database
    * @return     The newly inserted item
    */
  def create(item: Breeder.Create): Task[Breeder] = {
    post(Breeder(
      name = item.name,
      id   = UUID.randomUUID()))
  }

  /**
    * Updates a Breeder in the BreederCollection
    *
    * @param id       UUID of the Breeder to be replaced
    * @param modifier Modifier Breeder model to modify the Breeder in the database
    * @return         Newly replaced Breeder
    */
  def patchWithError(id: UUID, modifier: Breeder.Modify): Task[Either[BreederNotFound, Breeder]] = {
    for {
      item   <- getById(id)
      update <- item match {
        case Some(breeder) => replace(id, modifier.name.map(name => breeder.copy(name = name)).getOrElse(breeder)).map(Right(_))
        case None          => Task.eval(Left(BreederNotFound(id)))
      }
    } yield update
  }

  /**
    * Replaces a Breeder in the BreederCollection
    *
    * @param id      UUID of the Breeder to be updated
    * @param breeder Creator Breeder used to update the model in the database
    * @return        Newly updated Breeder
    */
  def putWithError(id: UUID, breeder: Breeder.Create): Task[Either[BreederNotFound, Breeder]] = {
    for {
      item   <- getById(id)
      update <- item match {
        case Some(_) => replace(id, Breeder(id, breeder.name)).map(Right(_))
        case None    => Task.eval(Left(BreederNotFound(id)))
      }
    } yield update
  }

  /**
    * Deletes a Breeder in the BreederCollection
    *
    * @param id UUID of the Breeder to be deleted
    * @return   Deleted Breeder
    */
  def deleteWithError(id: UUID): Task[Either[BreederNotFound, Breeder]] = {
    for {
      item   <- getById(id)
      update <- item match {
        case Some(_) => delete(id).map(Right(_))
        case None    => Task.eval(Left(BreederNotFound(id)))
      }
    } yield update
  }
}

object BreederCollection {
  final case class BreederNotFound(id: UUID) extends NotFound("Breeder", id)
}
