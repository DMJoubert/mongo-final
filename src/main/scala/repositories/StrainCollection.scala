package repositories

import java.util.UUID

import cats.implicits._
import models.Strain
import mongo.MongoCrudService
import monix.eval.Task
import monix.execution.Scheduler
import org.mongodb.scala.MongoDatabase
import repositories.BreederCollection.BreederNotFound
import repositories.StrainCollection.StrainNotFound
import repositories.errors.NotFound

class StrainCollection(database: MongoDatabase)
                      (implicit sch: Scheduler, breeders: BreederCollection) extends MongoCrudService[Strain] {

  override val collection = database.getCollection("strains")

  /**
    * Finds a Strain entry inside the StrainCollection and returns it if it is found otherwise returns a
    * StrainNotFound.
    *
    * @param id UUID of Strain to be found
    * @return   Either of the StrainNotFound or the Strain
    */
  def findByIdWithError(id: UUID): Task[Either[StrainNotFound, Strain]] = {
    getById(id).map {
      case Some(breeder) => Right(breeder)
      case None          => Left(StrainNotFound(id))
    }
  }

  /**
    * Inserts an item into the StrainCollection
    *
    * @param item Strain to be inserted into the collection
    * @return     Strain after it has been inserted otherwise a BreederNotFound if the supplied
    *             breederId does not already exist in the database
    */
  def create(item: Strain.Create): Task[Either[BreederNotFound, Strain]] = {
    for {
      breeder <- breeders.getById(item.breederId)
      update  <- breeder match {
        case Some(_) => post(Strain(UUID.randomUUID, item.name, item.breederId)).map(Right(_))
        case None    => Task.eval(Left(BreederNotFound(item.breederId)))
      }
    } yield update
  }

  /**
    * Patch an item in the StrainCollection
    *
    * @param id       ID of item to be patched
    * @param modifier Modifier Strain type that is used to modify the item
    * @return         The newly modified item or a StrainNotFound exception
    */
  def patchWithError(id: UUID, modifier: Strain.Modify): Task[Either[NotFound, Strain]] = {
    for {
      strain <- findByIdWithError(id)
      mod    <- modifier.breederId match {
        case Some(b) => breeders.findByIdWithError(b).map(_.map(_ => modifier))
        case None    => Task.eval(modifier.asRight[BreederNotFound])
      }
      update <- (strain, mod) match {
        case (Right(strain), Right(mod)) => replace(id, patchHelper(strain, mod)).map(_.asRight[NotFound])
        case (Right(_), Left(n))         => Task.eval(n.asLeft[Strain])
        case (Left(n), _)                => Task.eval(n.asLeft[Strain])
      }
    } yield update
  }

  private def patchHelper(toUpdate: Strain, modifier: Strain.Modify): Strain = {
    val t1 = modifier.name.map(name => toUpdate.copy(name = name)).getOrElse(toUpdate)
    modifier.breederId.map(id => toUpdate.copy(breederId = id)).getOrElse(t1)
  }

  /**
    * Replace an item in the StrainCollection
    *
    * @param id     ID of the item to be replaced
    * @param strain Strain creator used to replace the item in the database
    * @return       The newly replaced item in the database
    */
  def putWithError(id: UUID, strain: Strain.Create): Task[Either[NotFound, Strain]] = {
    for {
      strainValid <- findByIdWithError(id)
      updateValid <- breeders.findByIdWithError(strain.breederId)
      update      <- (strainValid, updateValid) match {
        case (Right(_), Right(_)) => replace(id, Strain(id, strain.name, strain.breederId)).map(_.asRight[NotFound])
        case (Right(_), Left(n))  => Task.eval(n.asLeft[Strain])
        case (Left(n), _)         => Task.eval(n.asLeft[Strain])
      }
    } yield update
  }

  /**
    * Deletes a strain in the StrainCollection
    *
    * @param id ID of the Strain to be deleted
    * @return   The deleted Strain
    */
  def deleteWithError(id: UUID): Task[Either[StrainNotFound, Strain]] = {
    for {
      item   <- getById(id)
      update <- item match {
        case Some(_) => delete(id).map(Right(_))
        case None    => Task.eval(Left(StrainNotFound(id)))
      }
    } yield update
  }
}

object StrainCollection {
  final case class StrainNotFound(id: UUID) extends NotFound("Strain", id)
}
