package repositories.errors

import java.util.UUID

/**
  * NotFound exception for Collections
  *
  * @param typeString String that defines the name of the type of Collection
  * @param id         ID of the item that cannot be found
  */
class NotFound(typeString: String, id: UUID) extends Exception(s"$typeString with ID ${id.toString} not found.")