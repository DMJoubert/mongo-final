package repositories

import java.time.OffsetDateTime
import java.util.UUID

import cats.implicits._
import models.Plant
import mongo.MongoCrudService
import monix.eval.Task
import monix.execution.Scheduler
import org.mongodb.scala.MongoDatabase
import repositories.PlantCollection.PlantNotFound
import repositories.StrainCollection.StrainNotFound
import repositories.errors.NotFound

class PlantCollection(database: MongoDatabase)
                     (implicit sch: Scheduler, strains: StrainCollection) extends MongoCrudService[Plant] {

  override val collection = database.getCollection("plants")

  /**
    * Adds a Plant to the PlantCollection
    *
    * @param plant Creator model for the Plant to be created
    * @return      Newly created Plant or a StrainNotFound error if the Strain inside the Creator
    *              model cannot be found
    */
  def create(plant: Plant.Create): Task[Either[StrainNotFound, Plant]] = {
    val date = plant.startDate match {
      case Some(d) => d
      case None    => OffsetDateTime.now
    }

    for {
      strain <- strains.getById(plant.strainId)
      update <- strain match {
        case Some(_) => post(Plant(id = UUID.randomUUID, strainId = plant.strainId, startDate = date)).map(Right(_))
        case None    => Task.eval(Left(StrainNotFound(plant.strainId)))
      }
    } yield update
  }

  /**
    * Finds a Plant inside the PlantCollection based on a provided UUID
    *
    * @param id UUID of the plant to be found
    * @return   Found Plant model or a PlantNotFound exception
    */
  def findByIdWithError(id: UUID): Task[Either[PlantNotFound, Plant]] = {
    getById(id).map {
      case Some(plant) => Right(plant)
      case None        => Left(PlantNotFound(id))
    }
  }

  /**
    * Modifies a Plant in the PlantCollection
    *
    * @param id       UUID of the Plant to be patched
    * @param modifier Plant Modifier model to modify the found plant
    * @return         The newly updated plant or a NotFound exception
    */
  def patchWithError(id: UUID, modifier: Plant.Modify): Task[Either[NotFound, Plant]] = {
    for {
      plant  <- findByIdWithError(id)
      mod    <- modifier.strainId match {
        case Some(s) => strains.findByIdWithError(s).map(_.map(_ => modifier))
        case None    => Task.eval(modifier.asRight[StrainNotFound])
      }
      update <- (plant, mod) match {
        case (Right(plant), Right(mod)) => replace(id, patchHelper(plant, mod)).map(_.asRight[NotFound])
        case (Right(_), Left(n))        => Task.eval(n.asLeft[Plant])
        case (Left(n), _)               => Task.eval(n.asLeft[Plant])
      }
    } yield update
  }

  private def patchHelper(toUpdate: Plant, modifier: Plant.Modify): Plant = {
    val t1 = modifier.strainId.map(sId => toUpdate.copy(strainId = sId)).getOrElse(toUpdate)
    modifier.startDate.map(sd => toUpdate.copy(startDate = sd)).getOrElse(t1)
  }

  /**
    * Replaces a Plant in the PlantCollection
    *
    * @param id    UUID of the Plant to be replaced
    * @param plant Creator Model of the Plant that the original Plant is replaced with
    * @return      Newly replaced Plant model or a NotFound exception
    */
  def putWithError(id: UUID, plant: Plant.Create): Task[Either[NotFound, Plant]] = {
    val date = plant.startDate match {
      case Some(d) => d
      case None    => OffsetDateTime.now
    }

    for {
      plantValid  <- findByIdWithError(id)
      updateValid <- strains.findByIdWithError(plant.strainId)
      update      <- (plantValid, updateValid) match {
        case (Right(_), Right(_)) => replace(id, Plant(id, plant.strainId, date)).map(_.asRight[NotFound])
        case (Right(_), Left(n))  => Task.eval(n.asLeft[Plant])
        case (Left(n), _)         => Task.eval(n.asLeft[Plant])
      }
    } yield update
  }

  /**
    * Deletes a Plant in the PlantCollection
    *
    * @param id UUID of the plant to be deleted
    * @return   Deleted Plant model or a PlantNotFound exception
    */
  def deleteWithError(id: UUID): Task[Either[PlantNotFound, Plant]] = {
    for {
      item   <- getById(id)
      update <- item match {
        case Some(_) => delete(id).map(Right(_))
        case None    => Task.eval(Left(PlantNotFound(id)))
      }
    } yield update
  }
}

object PlantCollection {
  final case class PlantNotFound(id: UUID) extends NotFound("Plant", id)
}
