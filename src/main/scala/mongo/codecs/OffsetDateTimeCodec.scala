package mongo.codecs

import java.time.{Instant, OffsetDateTime, ZoneOffset}

import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.{BsonReader, BsonWriter}

class OffsetDateTimeCodec extends Codec[OffsetDateTime] {
  override def decode(reader: BsonReader, decoderContext: DecoderContext): OffsetDateTime = {
    OffsetDateTime.ofInstant(Instant.ofEpochMilli(reader.readDateTime()), ZoneOffset.UTC)
  }

  override def encode(writer: BsonWriter, value: OffsetDateTime, encoderContext: EncoderContext): Unit = {
    writer.writeDateTime(value.toInstant.toEpochMilli)
  }

  override def getEncoderClass: Class[OffsetDateTime] = classOf[OffsetDateTime]
}