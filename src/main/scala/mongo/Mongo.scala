package mongo

import mongo.codecs.OffsetDateTimeCodec
import org.bson.UuidRepresentation
import org.bson.codecs.UuidCodecProvider
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.{DEFAULT_CODEC_REGISTRY, Macros}
import org.mongodb.scala.{MongoClient, MongoClientSettings, MongoDatabase, ServerAddress}

import scala.collection.JavaConverters._

object Mongo {
  private val strainCodecs = {
    import models.Strain
    import models.Strain._
    List(Macros.createCodecProviderIgnoreNone[Strain], Macros.createCodecProviderIgnoreNone[Project])
  }

  private val breederCodecs = {
    import models.Breeder
    import models.Breeder._
    List(Macros.createCodecProviderIgnoreNone[Breeder], Macros.createCodecProviderIgnoreNone[Project])
  }

  private val plantCodecs = {
    import models.Plant
    import models.Plant._
    List(Macros.createCodecProviderIgnoreNone[Plant], Macros.createCodecProviderIgnoreNone[Project])
  }

  private val codecRegistry = fromRegistries(
    CodecRegistries.fromCodecs(new OffsetDateTimeCodec),
    fromProviders(
      (breederCodecs ++ strainCodecs ++ plantCodecs
      ++ List(new UuidCodecProvider(UuidRepresentation.STANDARD))).asJava),
    DEFAULT_CODEC_REGISTRY)

  val settings: MongoClientSettings = MongoClientSettings.builder()
    .applyToClusterSettings(b => b.hosts(List(new ServerAddress("localhost")).asJava))
    .codecRegistry(codecRegistry)
    .build()

  val client: MongoClient = MongoClient(settings)
  val database: MongoDatabase = client.getDatabase("tester")
}