package mongo

import java.util.UUID

import com.mongodb.client.model.ReturnDocument
import crud._
import mongo.MongoImplicits._
import monix.eval.Task
import monix.execution.Scheduler
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.FindOneAndReplaceOptions
import org.mongodb.scala.model.Projections._

import scala.reflect.ClassTag

abstract class MongoCrudService[A : ClassTag](implicit sch: Scheduler) extends CrudService[A] {

  val collection: MongoCollection[A]
  private lazy val replaceOptions = new FindOneAndReplaceOptions().returnDocument(ReturnDocument.AFTER)

  /**
    * Returns the number of entries inside the MongoDB database
    *
    * @return A Long that contains the number of entries in the database
    */
  override def count(): Task[Long] = {
    collection.countDocuments.toTask
  }

  /**
    * Returns a record inside the database that matched the UUID of that record in the database
    *
    * @param id UUID of the record to be returned
    * @return   The retrieved record inside the database inside an Option
    */
  override def getById(id: UUID): Task[Option[A]] = {
    Task.deferFutureAction { implicit sch =>
      collection.find(equal("id", id)).headOption
    }
  }

  /**
    * Returns a projected record inside the database that matches the UUID of that record in the database.
    *
    * @param id     UUID of the record to be returned
    * @param fields Fields of the record that should be projected
    * @return       Projected record inside the database that matches the given UUID and fields
    */
  override def getById[B : ClassTag](id: UUID, fields: List[String]): Task[Option[B]] = {
    Task.deferFutureAction { implicit sch =>
      collection.find[B](equal("id", id)).projection(include(fields : _*)).headOption
    }
  }

  /**
    * Queries the database for all records and projects them according to the provided fields
    *
    * @param orderBy List of tuples containing the field to order by as well as an integer (-1, 1) that determines
    *                how the fields should be ordered
    * @param limit   Limits the number of records to be returned to the user. Defaults to 50.
    * @param offset  Chooses how many records to offset in the database before running the query, Defaults to 0.
    * @param fields  Determines which fields of the object should be projected back to the user.
    * @return        Queried records of the database.
    */
  override def getMany[B : ClassTag](orderBy: List[(String, Int)],
                                     limit:   Int = 50,
                                     offset:  Int = 0,
                                     fields:  List[String] = List.empty[String]): Task[(List[B], Long)] = {
    for {
      num <- count
      payload <- collection.find[B]
        .projection(include(fields : _*))
        .skip(offset)
        .order(orderBy)
        .limit(limit)
        .toTask
    } yield (payload.toList, num)
  }

  /**
    * Hard-deletes a record in the database that matches a given UUID
    *
    * @param id UUID of the record to be deleted
    * @return   Deleted record
    */
  override def delete(id: UUID): Task[A] = {
    Task.deferFutureAction { implicit sch =>
      collection.findOneAndDelete(equal("id", id)).head
    }
  }

  /**
    * Replaces a record in the database with a given UUID with another record of the same type
    *
    * @param id   UUID of the record that should be replaced
    * @param item Item to replace the old record
    * @return     Updated record in the database
    */
  override def replace(id: UUID, item: A): Task[A] = {
    Task.deferFutureAction { implicit sch =>
      collection.findOneAndReplace(equal("id", id), item, replaceOptions).head
    }
  }

  /**
    * Inserts an item into the database
    *
    * @param item Item to be inserted in the database
    * @return     The newly inserted item
    */
  override def post(item: A): Task[A] = {
    collection.insertOne(item).toTask.map { _ => item }
  }
}
