package mongo

import monix.eval.Task
import org.mongodb.scala.bson.conversions.Bson
import org.mongodb.scala.model.Sorts.{ascending, descending, orderBy}
import org.mongodb.scala.{FindObservable, SingleObservable}

trait MongoImplicits {

  /**
    * Helper for doing implicit operations on FindObservables
    *
    * @param obs FindObservable operator
    * @tparam A  The type of the FindObservable
    */
  implicit class FindObservableOps[A](obs: FindObservable[A]) {

    private def sortType(pair: (String, Int)): Bson = pair match {
      case (field, 1) => ascending(field)
      case (field, -1) => descending(field)
      case (field, _) => ascending(field)
    }

    def order(order: List[(String, Int)]): FindObservable[A] = order match {
      case Nil =>
        obs
      case h :: Nil =>
        obs.sort(sortType(h))
      case h =>
        obs.sort(orderBy(h.map(sortType): _*))
    }
  }

  /**
    * Helpers for converting SingleObservables into Tasks.
    *
    * @param obs The SingleObservable to convert to a Task.
    * @tparam A The type that the SingleObservable will emit.
    */
  implicit class SingleObservableToTask[A](obs: SingleObservable[A]) {
    def map[B](f: A => B): Task[B] = Task.deferFutureAction(implicit s => obs.toFuture).map(f)

    def flatMap[B](f: A => Task[B]): Task[B] =
      Task.deferFutureAction(implicit s => obs.toFuture).flatMap(f)

    def toTask: Task[A] = Task.deferFutureAction(implicit s => obs.toFuture)
  }

  /**
    * Helpers for converting FindObservables into Tasks.
    *
    * @param obs The FindObservable to convert to a Task.
    * @tparam A The type of element that the FindObservable will emit.
    */
  implicit class FindObservableToTask[A](obs: FindObservable[A]) {
    def map[B](f: Seq[A] => B): Task[B] = Task.deferFutureAction(implicit s => obs.toFuture).map(f)

    def flatMap[B](f: Seq[A] => Task[B]): Task[B] =
      Task.deferFutureAction(implicit s => obs.toFuture).flatMap(f)

    def toTask: Task[Seq[A]] = Task.deferFutureAction(implicit s => obs.toFuture)
  }
}

object MongoImplicits extends MongoImplicits
