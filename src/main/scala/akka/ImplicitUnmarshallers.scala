package akka

import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.util.FastFuture

trait ImplicitUnmarshallers {
  /**
    * Alternative implemetation of CsvSeq that instead returns a List[T] instead of Seq[T]
    *
    * @param unmarshaller Specific Unmarshaller for the T desired
    * @return             The new Unmarshaller
    */
  implicit def csvList[T](implicit unmarshaller: Unmarshaller[String, T]): Unmarshaller[String, List[T]] = {
    Unmarshaller.strict[String, List[String]] { string =>
      string.split(",").map(_.trim).toList
    }.flatMap { implicit ec => implicit mat => strings =>
      FastFuture.sequence(strings.map(unmarshaller(_)))
    }
  }

  /**
    * Base unmarshaller for Strings that are delimited by '
    */
  implicit val pairSeq: Unmarshaller[String, (String, Int)] = {
    Unmarshaller.strict[String, (String, Int)] { string =>
      val Array(a, b) = string.split("\'")
      (a, b.toInt)
    }
  }
}

object ImplicitUnmarshallers extends ImplicitUnmarshallers
