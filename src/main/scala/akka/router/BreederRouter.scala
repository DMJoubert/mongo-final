package akka.router

import java.util.UUID

import akka.ImplicitUnmarshallers.{csvList, pairSeq}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpjson4s.Json4sSupport._
import models.{Breeder, Metadata, Response}
import monix.execution.Scheduler
import org.json4s.{DefaultFormats, native}
import repositories.BreederCollection
import utilities.UuIdSerializer

import scala.util.{Failure, Success}

/**
  * Class that is used to create a BreederCollection Router that interacts with MongoDB through Akka-HTTP
  *
  * @param breederCollection BreederCollection to be routed
  */
class BreederRouter(implicit breederCollection: BreederCollection, sch: Scheduler) extends Router with Directives {
  override def route: Route = {
    implicit val serialization = native.Serialization
    implicit val formats = DefaultFormats + UuIdSerializer

    pathPrefix("breeders") {
      pathEndOrSingleSlash {
        get {
          parameters(
            'orderBy.as(csvList[(String, Int)]).?(List.empty[(String, Int)]),
            'limit.as[Int].?(50),
            'offset.as[Int].?(0),
            'fields.as(csvList[String]).?(List.empty[String])) { (orderBy, limit, offset, fields) =>
            onComplete(breederCollection.getMany[Breeder.Project](orderBy, limit, offset, fields).runToFuture) {
              case Success((breeders, count)) =>
                complete(Response.list(breeders, Metadata.OK(count)))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        } ~
        post {
          entity(as[Breeder.Create]) { createBreeder =>
            onComplete(breederCollection.create(createBreeder).runToFuture) {
              case Success(breeder) =>
                complete(
                  StatusCodes.Created,
                  List(Location(s"/breeders/${breeder.id}")),
                  Response.single(breeder, Metadata.Created))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        }
      } ~
      path(JavaUUID) { id: UUID =>
        patch {
          entity(as[Breeder.Modify]) { updateBreeder =>
            onComplete(breederCollection.patchWithError(id, updateBreeder).runToFuture) {
              case Success(Right(breeder)) =>
                complete(Response.single(breeder, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        } ~
        get {
          onComplete(breederCollection.findByIdWithError(id).runToFuture) {
            case Success(Right(breeder)) =>
              complete(Response.single(breeder, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound))
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError))
          }
        } ~
        delete {
          onComplete(breederCollection.deleteWithError(id).runToFuture) {
            case Success(Right(breeder)) =>
              complete(Response.single(breeder, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound))
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError))
          }
        } ~
        put {
          entity(as[Breeder.Create]) { putBreeder =>
            onComplete(breederCollection.putWithError(id, putBreeder).runToFuture) {
              case Success(Right(breeder)) =>
                complete(Response.single(breeder, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        }
      }
    }
  }
}