package akka.router

import akka.http.scaladsl.server.Route

trait Router {

  /**
    * Returns the route created in the akka.Router
    *
    * @return The created Route
    */
  def route: Route
}
