package akka.router

import java.util.UUID

import akka.ImplicitUnmarshallers.{csvList, _}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.headers.Location
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpjson4s.Json4sSupport._
import models.{Metadata, Response, Strain}
import monix.execution.Scheduler
import org.json4s.{DefaultFormats, native}
import repositories.StrainCollection
import utilities.UuIdSerializer

import scala.util.{Failure, Success}

class StrainRouter(implicit strainCollection: StrainCollection, sch: Scheduler) extends Router with Directives {
  override def route: Route = {
    implicit val serialization = native.Serialization
    implicit val formats = DefaultFormats + UuIdSerializer

    pathPrefix("strains") {
      pathEndOrSingleSlash {
        get {
          parameters(
            'orderBy.as(csvList[(String, Int)]).?(List.empty[(String, Int)]),
            'limit.as[Int].?(50),
            'offset.as[Int].?(0),
            'fields.as(csvList[String]).?(List.empty[String])) { (orderBy, limit, offset, fields) =>
            onComplete(strainCollection.getMany[Strain.Project](orderBy, limit, offset, fields).runToFuture) {
              case Success((strains, count)) =>
                complete(Response.list(strains, Metadata.OK(count)))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        } ~
        post {
          entity(as[Strain.Create]) { createStrain =>
            onComplete(strainCollection.create(createStrain).runToFuture) {
              case Success(Right(strain)) =>
                complete(
                  StatusCodes.Created,
                  List(Location(s"/strains/${strain.id}")),
                  Response.single(strain, Metadata.Created))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound)
                )
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        }
      } ~
      path(JavaUUID) { id: UUID =>
        patch {
          entity(as[Strain.Modify]) { updateStrain =>
            onComplete(strainCollection.patchWithError(id, updateStrain).runToFuture) {
              case Success(Right(strain)) =>
                complete(Response.single(strain, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        } ~
        get {
          onComplete(strainCollection.findByIdWithError(id).runToFuture) {
            case Success(Right(strain)) =>
              complete(Response.single(strain, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound))
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError))
          }
        } ~
        put {
          entity(as[Strain.Create]) { putStrain =>
            onComplete(strainCollection.putWithError(id, putStrain).runToFuture) {
              case Success(Right(strain)) =>
                complete(Response.single(strain, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError))
            }
          }
        } ~
        delete {
          onComplete(strainCollection.deleteWithError(id).runToFuture) {
            case Success(Right(strain)) =>
              complete(Response.single(strain, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound))
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError))
          }
        }
      }
    }
  }
}
