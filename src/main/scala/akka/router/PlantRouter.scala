package akka.router

import java.util.UUID

import akka.ImplicitUnmarshallers._
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.{Directives, Route}
import de.heikoseeberger.akkahttpjson4s.Json4sSupport._
import models.{Metadata, Plant, Response}
import monix.execution.Scheduler
import org.json4s.{DefaultFormats, native}
import repositories.PlantCollection
import utilities.{OffsetDateTimeSerializer, UuIdSerializer}

import scala.util.{Failure, Success}

class PlantRouter(implicit sch: Scheduler, plants: PlantCollection) extends Router with Directives {
  override def route: Route = {
    implicit val serialization = native.Serialization
    implicit val formats = DefaultFormats + UuIdSerializer + OffsetDateTimeSerializer

    pathPrefix("plants") {
      pathEndOrSingleSlash {
        get {
          parameters(
            'orderBy.as(csvList[(String, Int)]).?(List.empty[(String, Int)]),
            'limit.as[Int].?(50),
            'offset.as[Int].?(0),
            'fields.as(csvList[String]).?(List.empty[String])) { (orderBy, limit, offset, fields) =>
            onComplete(plants.getMany[Plant.Project](orderBy, limit, offset, fields).runToFuture) {
              case Success((plants, count)) =>
                complete(Response.list(plants, Metadata.OK(count)))
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError)
                )
            }
          }
        } ~
        post {
          entity(as[Plant.Create]) { creator =>
            onComplete(plants.create(creator).runToFuture) {
              case Success(Right(plant)) =>
                complete(
                  StatusCodes.Created,
                  Response.single(plant, Metadata.OK)
                )
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound)
                )
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError)
                )
            }
          }
        }
      } ~
      path(JavaUUID) { id: UUID =>
        get {
          onComplete(plants.findByIdWithError(id).runToFuture) {
            case Success(Right(plant)) =>
              complete(Response.single(plant, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound)
              )
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError)
              )
          }
        } ~
        patch {
          entity(as[Plant.Modify]) { modifier =>
            onComplete(plants.patchWithError(id, modifier).runToFuture) {
              case Success(Right(plant)) =>
                complete(Response.single(plant, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound)
                )
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError)
                )
            }
          }
        } ~
        put {
          entity(as[Plant.Create]) { creator =>
            onComplete(plants.putWithError(id, creator).runToFuture) {
              case Success(Right(plant)) =>
                complete(Response.single(plant, Metadata.OK))
              case Success(Left(e)) =>
                complete(
                  StatusCodes.NotFound,
                  Response.single(e.getMessage, Metadata.NotFound)
                )
              case Failure(e) =>
                complete(
                  StatusCodes.InternalServerError,
                  Response.single(e.getMessage, Metadata.InternalServerError)
                )
            }
          }
        } ~
        delete {
          onComplete(plants.deleteWithError(id).runToFuture) {
            case Success(Right(plant)) =>
              complete(Response.single(plant, Metadata.OK))
            case Success(Left(e)) =>
              complete(
                StatusCodes.NotFound,
                Response.single(e.getMessage, Metadata.NotFound)
              )
            case Failure(e) =>
              complete(
                StatusCodes.InternalServerError,
                Response.single(e.getMessage, Metadata.InternalServerError)
              )
          }
        }
      }
    }
  }
}
