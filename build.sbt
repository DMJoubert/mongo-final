name := "mongo-final"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.21",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.21" % Test,
  "com.typesafe.akka" %% "akka-stream" % "2.5.21",
  "com.typesafe.akka" %% "akka-stream-testkit" % "2.5.21" % Test,
  "com.typesafe.akka" %% "akka-http" % "10.1.7",
  "com.typesafe.akka" %% "akka-http-testkit" % "10.1.7" % Test,

  "de.heikoseeberger" %% "akka-http-json4s" % "1.25.2",
  "org.json4s" %% "json4s-native" % "3.6.4",

  "org.scalatest" %% "scalatest" % "3.0.4" % Test,
  "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.2",
  "io.monix" %% "monix" % "3.0.0-RC2"
)